package com.mygdx.game.modelo;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ladrillo extends Personaje{
    public boolean activo;
    public int aguante;
    public boolean animacion;
    public boolean[] objetos;
    public poder powerR;
    public poder powerA;
    public corazon heart;
    public flama ataque;

    public ladrillo(Texture textura, float X, float Y, float width, float height) {
        super(textura, X, Y, width, height);
        activo = true;
        animacion = true;
        objetos = new boolean[4];
        objetos[0] = false;
        objetos[1] = false;
        objetos[2] = false;
        objetos[3] = false;

    }
}
