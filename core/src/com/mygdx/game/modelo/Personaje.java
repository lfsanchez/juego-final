package com.mygdx.game.modelo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class Personaje {
    Animation animacion;
    public Sprite personaje;
    public boolean animado;

    public Personaje(Texture textura, float X, float Y, float width, float height){
        personaje = new Sprite(textura);
        personaje.setPosition(X, Y);
        personaje.setSize(width, height);
        animado = false;
    }

    public void animacion(Texture textura, int partesX, int partesY){
        TextureRegion[][] texturas = TextureRegion.split(textura,textura.getWidth()/partesX, textura.getHeight()/partesY);
        if(partesY>1){
            TextureRegion[] aux = new TextureRegion[partesY*partesX];
            for(int i=0; i<partesY;i++){
                System.arraycopy(texturas[0], 0, aux, i * partesX, partesX);
            }
            animacion = new Animation(1/25f, aux);
        }
        else{
            animacion = new Animation(1/25f, TextureRegion.split(textura,textura.getWidth()/partesX, textura.getHeight())[0]);
        }
        personaje.setRegion((TextureRegion) animacion.getKeyFrame(0f, true));
        animado = true;
    }

    public void quieto(Texture textura){
        personaje.setRegion(textura);
        animado = false;
    }

    public void render(SpriteBatch batch, float tiempo){
        if(animado){
            personaje.setRegion((TextureRegion) animacion.getKeyFrame(tiempo, true));
        }
        personaje.draw(batch);
    }
}
