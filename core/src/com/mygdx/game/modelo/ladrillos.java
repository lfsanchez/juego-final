package com.mygdx.game.modelo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.FileHandler;


public class ladrillos{
    public ArrayList<ladrillo> mapa;

    public ladrillos(String name, Graphics.DisplayMode dm){
        mapa = new ArrayList<>();
        FileHandle handle = Gdx.files.internal(name);
        String[] br = handle.readString().split("\r\n");
        int j = 0;
        String line;
        while (j<br.length) {
            int i = 0;
            line = br[j];
            while(i<line.length()){
                if(line.charAt(i) != ' '){
                    Texture t = new Texture("ladrillos.jpg");
                    ladrillo aux = new ladrillo(t,i*dm.width/25, dm.height*0.99f - (j+1)*dm.height/20, dm.width/25, dm.height/20);
                    pintar(line.charAt(i), aux, t);
                }
                i++;
            }
            j++;
        }
    }

    public void renderObjects(SpriteBatch batch, float tiempo){
        for(int i=0; i<mapa.size(); i++){
            if(mapa.get(i).objetos[0]){
               mapa.get(i).heart.render(batch, tiempo);
            }
            if(mapa.get(i).objetos[1]){
                mapa.get(i).powerR.render(batch,tiempo);
            }
            if(mapa.get(i).objetos[2]){
                mapa.get(i).powerA.render(batch, tiempo);
            }
            if(mapa.get(i).objetos[3]){
                mapa.get(i).ataque.render(batch,tiempo);
            }
        }
    }

    public void pintar(char val, ladrillo aux, Texture t){
        switch (val){
            case 'r':
                aux.personaje.setRegion(new TextureRegion(t,0,0,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 1;
                mapa.add(aux);
                break;
            case 'o':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()/3,0,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 2;
                mapa.add(aux);
                break;
            case 'y':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()*2/3,0,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 3;
                mapa.add(aux);
                break;
            case 'g':
                aux.personaje.setRegion(new TextureRegion(t,0,t.getHeight()/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 4;
                mapa.add(aux);
                break;
            case 'a':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()/3,t.getHeight()/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 5;
                mapa.add(aux);
                break;
            case 'c':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()*2/3,t.getHeight()/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 6;
                mapa.add(aux);
                break;
            case 'b':
                aux.personaje.setRegion(new TextureRegion(t,0,t.getHeight()*2/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 7;
                mapa.add(aux);
                break;
            case 'm':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()/3,t.getHeight()*2/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 8;
                mapa.add(aux);
                break;
            case 'v':
                aux.personaje.setRegion(new TextureRegion(t,t.getWidth()*2/3,t.getHeight()*2/3,t.getWidth()/3, t.getHeight()/3));
                aux.aguante = 9;
                mapa.add(aux);
                break;
            case 'n':
                aux.personaje.setRegion(new TextureRegion(t,0,0,0.0001f, 0.0001f));
                aux.aguante = 10;
                mapa.add(aux);
                break;
        }
    }

    public void valor(int valor, ladrillo aux, Texture t){
        switch (valor){
            case 1:
                pintar('r', aux, t);
                break;
            case 2:
                pintar('o', aux, t);
                break;
            case 3:
                pintar('y', aux, t);
                break;
            case 4:
                pintar('g', aux, t);
                break;
            case 5:
                pintar('a', aux, t);
                break;
            case 6:
                pintar('c', aux, t);
                break;
            case 7:
                pintar('b', aux, t);
                break;
            case 8:
                pintar('m', aux, t);
                break;
            case 9:
                pintar('v', aux, t);
                break;
        }
    }
}
