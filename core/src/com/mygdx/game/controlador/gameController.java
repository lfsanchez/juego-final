package com.mygdx.game.controlador;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.mygdx.game.modelo.Personaje;
import com.mygdx.game.modelo.corazon;
import com.mygdx.game.modelo.flama;
import com.mygdx.game.modelo.ladrillo;
import com.mygdx.game.modelo.ladrillos;
import com.mygdx.game.modelo.pelota;
import com.mygdx.game.modelo.plataforma;
import com.mygdx.game.modelo.poder;

import java.util.Random;

public class gameController {
    public int nivel;
    int moveX;
    int moveY;
    Music golpe;
    Music boing;
    Music explosion;
    Music muerte;

    public gameController(int nivel){
       this.nivel = nivel;
       moveX = nivel+3;
       moveY = nivel+3;
       golpe = Gdx.audio.newMusic(Gdx.files.internal("golpe.mp3"));
       boing = Gdx.audio.newMusic(Gdx.files.internal("boing.mp3"));
       explosion = Gdx.audio.newMusic(Gdx.files.internal("boom.mp3"));
       muerte = Gdx.audio.newMusic(Gdx.files.internal("muerte.mp3"));
    }

    public Object[] cambios(Object[] personajes, Graphics.DisplayMode dm){
        pelota ball = (pelota) personajes[1];
        plataforma platform = (plataforma) personajes[0];
        ladrillos pared = (ladrillos) personajes[2];
        int vidas = (int) personajes[3];
        int puntos = (int) personajes[4];
        if(ball.animado) {
            if (ball.personaje.getY() <= 0) {
                muerte.play();
            }
            if (ball.personaje.getY() + ball.personaje.getHeight() <= 0) {
                muerte.play();
                ball.quieto(new Texture("goopy_quieto.png"));
                ball.personaje.setPosition(dm.width/2 - dm.width * 0.0125f/2, dm.height*0.025f + dm.height*0.025f);
                platform.personaje.setPosition(dm.width/2 - dm.width*0.15f/2, dm.height*0.025f);
                ball.count = 0;
                ball.poder =false;
                platform.poder = false;
                platform.count = 0;
                moveY = nivel+3;
                vidas--;
            }
            else{
                if (ball.personaje.getX() + ball.personaje.getWidth() / 2 >= platform.personaje.getX() && ball.personaje.getX() + ball.personaje.getWidth() / 2 <= platform.personaje.getX() + platform.personaje.getWidth() && ball.personaje.getY() <= platform.personaje.getY() + dm.height*0.025 && ball.personaje.getY()>= platform.personaje.getY()) {
                    if(platform.poder && platform.count > 0){
                        ball.quieto(new Texture("goopy_quieto.png"));
                        ball.personaje.setPosition(platform.personaje.getX()+platform.personaje.getWidth()/2-ball.personaje.getWidth()/2, platform.personaje.getY() + dm.height*0.025f);
                        moveY = nivel+3;
                        platform.count--;
                        if(platform.count<=0){
                            platform.poder = false;
                        }
                    }
                    else{
                        moveY = nivel+3;
                        boing.play();
                    }
                }
                int i = 0;
                boolean sigue = true;
                ladrillo actual;
                while (i < pared.mapa.size() && sigue) {
                    actual = pared.mapa.get(i);
                    if (actual.activo&&ball.personaje.getX() + ball.personaje.getWidth() >= actual.personaje.getX() && ball.personaje.getX() <= actual.personaje.getX() + actual.personaje.getWidth()&&ball.personaje.getY()+ball.personaje.getHeight()>=actual.personaje.getY()&&ball.personaje.getY()<= actual.personaje.getY()+actual.personaje.getHeight()) {
                        Random aleatorio = new Random(System.currentTimeMillis());
                        int al = aleatorio.nextInt(101);
                        if(al >= 95 && actual.objetos[0] != true){
                            actual.objetos[0] = true;
                            Texture pw = new Texture("corazon.png");
                            actual.heart = new corazon(pw,0,0,pw.getWidth(), pw.getHeight());
                            actual.heart.animacion(pw,6,1);
                            actual.heart.personaje.setSize(dm.height*0.07f,dm.height*0.07f);
                            actual.heart.personaje.setPosition(actual.personaje.getX()+actual.personaje.getWidth()/2 - actual.heart.personaje.getWidth()/2, actual.personaje.getY() - actual.heart.personaje.getHeight());
                        }
                        else{
                            if(al >= 90 && actual.objetos[1] != true){
                                actual.objetos[1] = true;
                                Texture pw = new Texture("poderojo.png");
                                actual.powerR = new poder(pw,0,0,pw.getWidth(), pw.getHeight());
                                actual.powerR.animacion(pw,12,1);
                                actual.powerR.personaje.setSize(dm.width * 0.025f, dm.width * 0.025f);
                                actual.powerR.personaje.setPosition(actual.personaje.getX()+actual.personaje.getWidth()/2 - actual.powerR.personaje.getWidth()/2, actual.personaje.getY() - actual.powerR.personaje.getHeight());
                            }
                            else{
                                if(al>=85 && actual.objetos[2] != true){
                                    actual.objetos[2] = true;
                                    Texture pw = new Texture("poderazul.png");
                                    actual.powerA = new poder(pw,0,0,pw.getWidth(), pw.getHeight());
                                    actual.powerA.animacion(pw,22,1);
                                    actual.powerA.personaje.setSize(dm.width * 0.025f, dm.width * 0.025f);
                                    actual.powerA.personaje.setPosition(actual.personaje.getX()+actual.personaje.getWidth()/2 - actual.powerA.personaje.getWidth()/2, actual.personaje.getY() - actual.powerA.personaje.getHeight());

                                }
                                else{
                                    if(al>=85 && actual.objetos[3] != true){
                                        actual.objetos[3] = true;
                                        Texture pw = new Texture("fuego.png");
                                        actual.ataque = new flama(pw,0,0,pw.getWidth(), pw.getHeight());
                                        actual.ataque.animacion(pw,14,1);
                                        actual.ataque.personaje.setSize(dm.height*0.07f,dm.height*0.1f);
                                        actual.ataque.personaje.setPosition(actual.personaje.getX()+actual.personaje.getWidth()/2 - actual.ataque.personaje.getWidth()/2, actual.personaje.getY() - actual.ataque.personaje.getHeight());
                                    }
                                }
                            }
                        }
                        if(ball.count>0 && ball.poder){
                            if(ball.count>=actual.aguante){
                                puntos += actual.aguante;
                                ball.count -= actual.aguante;
                                actual.animacion(new Texture("explosion.png"),5,2);
                                explosion.play();
                                actual.activo = false;
                                actual.aguante = 0;
                            }
                            else
                            {
                                actual.aguante -= ball.count;
                                puntos += ball.count;
                                ball.count = 0;
                                pared.valor(actual.aguante,actual,actual.personaje.getTexture());
                                golpe.play();
                            }
                            if(ball.count<=0){
                                ball.poder = false;
                                ball.animacion(new Texture("goopy.png"),17,1);
                            }
                        }
                        else{
                            if(actual.activo && actual.aguante>1){
                                actual.aguante--;
                                pared.valor(actual.aguante,actual,actual.personaje.getTexture());
                                golpe.play();
                            }
                            else{
                                if(actual.aguante == 1){
                                    actual.animacion(new Texture("explosion.png"),5,2);
                                    explosion.play();
                                    actual.activo = false;
                                }
                            }
                            puntos++;
                        }
                        if(ball.personaje.getY()+ball.personaje.getHeight()>=actual.personaje.getY()&&ball.personaje.getY()+ball.personaje.getHeight()<=actual.personaje.getY()+actual.personaje.getHeight()/4){
                            moveY = -nivel -3;
                            ball.personaje.setY(actual.personaje.getY()-ball.personaje.getHeight());
                        }
                        else{
                            if(ball.personaje.getY()<=actual.personaje.getY()+actual.personaje.getHeight()&&ball.personaje.getY()>=actual.personaje.getY()+actual.personaje.getHeight()*3/4){
                                moveY = nivel +3;
                                ball.personaje.setY(actual.personaje.getY()+actual.personaje.getHeight());
                            }
                            else{
                                if(ball.personaje.getWidth()+ball.personaje.getX()>=actual.personaje.getX()&&ball.personaje.getWidth()+ball.personaje.getX()<=actual.personaje.getX()+actual.personaje.getWidth()/3){
                                    moveX = -nivel -3;
                                    ball.personaje.setX(actual.personaje.getX()-ball.personaje.getWidth());
                                }
                                else{
                                    if(ball.personaje.getX()<=actual.personaje.getX()+actual.personaje.getWidth()&&ball.personaje.getX()>=actual.personaje.getX()+actual.personaje.getWidth()*2/3){
                                        moveX = nivel + 3;
                                        ball.personaje.setX(actual.personaje.getX()+actual.personaje.getWidth());
                                    }
                                    else{
                                        moveX *= -1;
                                    }
                                }
                            }
                        }
                        sigue = false;
                    }
                    i++;
                }
                if(ball.personaje.getX()<=1){
                    moveX = nivel+3;
                    golpe.play();
                }
                if(ball.personaje.getX()+ball.personaje.getWidth()>= dm.width-1){
                    moveX = -nivel-3;
                    golpe.play();
                }

                if(ball.personaje.getY()+ball.personaje.getHeight() >= dm.height-1){
                    moveY = -nivel-3;
                    golpe.play();
                }
                ball.personaje.translate(moveX,moveY);
            }
        }
        personajes[0] = platform;
        personajes[1] = ball;
        personajes[2] = pared;
        personajes[3] = vidas;
        personajes[4] = puntos;
        return personajes;
    }

    public Object[] objetosL(Object[] values, int pos, int vel, float height){
        ladrillo actual = (ladrillo) values[0];
        pelota bola = (pelota) values[1];
        plataforma platform = (plataforma) values[2];
        int vidas = (int) values[3];
        switch (pos){
            case 0:
                if(actual.heart.personaje.getY() > 0){
                    if(actual.heart.personaje.getY()+actual.heart.personaje.getHeight()/2 >= platform.personaje.getY() && actual.heart.personaje.getY()+actual.heart.personaje.getHeight()/2<= platform.personaje.getY()+height&&actual.heart.personaje.getX()+actual.heart.personaje.getWidth()/2>=platform.personaje.getX()&& actual.heart.personaje.getX()+actual.heart.personaje.getWidth()/2<=platform.personaje.getX()+platform.personaje.getWidth()){
                        actual.objetos[0] = false;
                        if(vidas < 5){
                            vidas++;
                        }
                    }
                    else{
                        actual.heart.personaje.translateY(-vel);
                    }
                }
                else{
                    actual.objetos[0] = false;
                }
                break;
            case 1:
                if(actual.powerR.personaje.getY() > 0){
                    if(actual.powerR.personaje.getY()+actual.powerR.personaje.getHeight()/2 >= platform.personaje.getY() && actual.powerR.personaje.getY()+actual.powerR.personaje.getHeight()/2<= platform.personaje.getY()+height&&actual.powerR.personaje.getX()+actual.powerR.personaje.getWidth()/2>=platform.personaje.getX()&& actual.powerR.personaje.getX()+actual.powerR.personaje.getWidth()/2<=platform.personaje.getX()+platform.personaje.getWidth()){
                        actual.objetos[1] = false;
                        bola.poder = true;
                        bola.count += 5;
                        bola.animacion(new Texture("goopy_quieto.png"), 1,1);
                    }
                    else{
                        actual.powerR.personaje.translateY(-vel);
                    }
                }
                else{
                    actual.objetos[1] = false;
                }
                break;
            case 2:
                if(actual.powerA.personaje.getY() > 0){
                    if(actual.powerA.personaje.getY()+actual.powerA.personaje.getHeight()/2 >= platform.personaje.getY() && actual.powerA.personaje.getY()+actual.powerA.personaje.getHeight()/2<= platform.personaje.getY()+height&&actual.powerA.personaje.getX()+actual.powerA.personaje.getWidth()/2>=platform.personaje.getX()&& actual.powerA.personaje.getX()+actual.powerA.personaje.getWidth()/2<=platform.personaje.getX()+platform.personaje.getWidth()){
                        actual.objetos[2] = false;
                        platform.poder = true;
                        platform.count += 5;
                    }
                    else{
                        actual.powerA.personaje.translateY(-vel);
                    }
                }
                else{
                    actual.objetos[2] = false;
                }
                break;
            case 3:
                if(actual.ataque.personaje.getY() > 0){
                    if(actual.ataque.personaje.getY()+actual.ataque.personaje.getHeight()/2 >= platform.personaje.getY() && actual.ataque.personaje.getY()+actual.ataque.personaje.getHeight()/2<= platform.personaje.getY()+platform.personaje.getHeight()&&actual.ataque.personaje.getX()+actual.ataque.personaje.getWidth()/2>=platform.personaje.getX()&& actual.ataque.personaje.getX()+actual.ataque.personaje.getWidth()/2<=platform.personaje.getX()+platform.personaje.getWidth()){
                        actual.objetos[3] = false;
                        if(vidas > 0){
                            vidas--;
                            muerte.play();
                        }
                    }
                    else{
                        actual.ataque.personaje.translateY(-vel);
                    }
                }
                else{
                    actual.objetos[3] = false;
                }
                break;
        }
        values[0] = actual;
        values[1] = bola;
        values[2] = platform;
        values[3] = vidas;
        return values;
    }

}
