package com.mygdx.game.controlador;

import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.game.modelo.ladrillos;
import com.mygdx.game.modelo.pelota;
import com.mygdx.game.modelo.plataforma;

public class playerController implements ControllerListener, InputProcessor {
    public int nivel;
    boolean control;
    int[] changes;
    int moveX;
    int moveY;
    int vidas;

    public playerController(int nivel, int vidas){
        Controllers.addListener(this);
        this.nivel = nivel;
        changes = new int[2];
        changes[0] = -1;
        changes[1] = -1;
        moveX = nivel+3;
        moveY = nivel+3;
        this.vidas = vidas;
    }

    public Object[] cambios(Object[] personajes, Graphics.DisplayMode dm){
        pelota ball = (pelota) personajes[1];
        plataforma platform = (plataforma) personajes[0];
        ladrillos pared = (ladrillos) personajes[2];
        int vidas = (int) personajes[3];
        if(vidas<this.vidas){
            changes[0] = -1;
            changes[1] = -1;
            this.vidas = vidas;
        }
        switch (changes[0]){
            case 0:
                if(platform.personaje.getX()>=0 && platform.personaje.getX()+platform.personaje.getWidth()<=dm.width){
                    if(changes[1] == 13){
                        platform.personaje.translate(-10-nivel,0);
                        if(!ball.animado){
                            ball.personaje.translate(-10-nivel,0);
                        }
                    }
                    else{
                        if(changes[1] == 14){
                            platform.personaje.translate(10+nivel,0);
                            if(!ball.animado){
                                ball.personaje.translate(10+nivel, 0);
                            }
                        }
                        if(changes[1]== 0){
                            if(!ball.animado){
                                ball.animacion(new Texture("goopy.png"),17,1);
                                moveY = nivel+3;
                                ball.personaje.translate(moveX,moveY);
                            }
                            changes[0] = -1;
                            changes[1] = -1;
                        }
                    }
                }
                else{
                    if(platform.personaje.getX() <= 0){
                        platform.personaje.translateX(1);
                        if(!ball.animado){
                            ball.personaje.translateX(1);
                        }
                    }
                    if(platform.personaje.getX()+platform.personaje.getWidth()>=dm.width){
                        platform.personaje.translateX(-1);
                        if(!ball.animado){
                            ball.personaje.translateX(-1);
                        }
                    }
                }
                break;
            case 1:
                if(platform.personaje.getX()>=0 && platform.personaje.getX()+platform.personaje.getWidth()<=dm.width){
                    if(changes[1]<0){
                        platform.personaje.translate(-10-nivel,0);
                        if(!ball.animado){
                            ball.personaje.translate(-10-nivel,0);
                        }
                    }
                    else{
                        platform.personaje.translate(10+nivel,0);
                        if(!ball.animado){
                            ball.personaje.translate(10+nivel, 0);
                        }
                    }
                }
                else{
                    if(platform.personaje.getX() <= 0){
                        platform.personaje.translateX(1);
                        if(!ball.animado){
                            ball.personaje.translateX(1);
                        }
                    }
                    if(platform.personaje.getX()+platform.personaje.getWidth()>=dm.width){
                        platform.personaje.translateX(-1);
                        if(!ball.animado){
                            ball.personaje.translateX(-1);
                        }
                    }
                }
                break;
            case 2:
                if(!ball.animado ){
                    ball.animacion(new Texture("goopy.png"),17,1);
                    moveY = nivel+3;
                    ball.personaje.translate(moveX,moveY);
                }
                break;
            case 3:
                if(platform.personaje.getX()>=0 && platform.personaje.getX()+platform.personaje.getWidth()<=dm.width&&changes[1]-platform.personaje.getWidth()/2>=0&&changes[1]+platform.personaje.getWidth()/2<=dm.width){
                    platform.personaje.setPosition(changes[1]-platform.personaje.getWidth()/2, platform.personaje.getY());
                    if(!ball.animado){
                        ball.personaje.setPosition(changes[1]-ball.personaje.getWidth()/2, ball.personaje.getY());
                    }
                    changes[0] = -1;
                    changes[1] = -1;
                }
                break;
        }
        personajes[0] = platform;
        personajes[1] = ball;
        personajes[2] = pared;
        return personajes;
    }

    @Override
    public void connected(Controller controller) {
    }

    @Override
    public void disconnected(Controller controller) {
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        control = true;
        if(buttonCode == 0 || buttonCode == 13 || buttonCode == 14){
            changes[0] = 0;
            changes[1] = buttonCode;
        }
        return true;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        control = true;
        if(axisCode == 0 && (value < -0.90 || value > 0.90)){
            changes[0] = 1;
            changes[1] = (int) (5 * value);
        }
        return true;
    }

    @Override
    public boolean keyDown(int keycode) { return false; }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        control = false;
        changes[0] = 2;
        changes[1] = screenX;
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        control = false;
        control = false;
        changes[0] = 3;
        changes[1] = screenX;
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}
