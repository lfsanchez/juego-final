package com.mygdx.game.pantalla;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.controlador.gameController;


public class ProyectoGrafica extends Game {

	private SpriteBatch batch;
	private SplashScreen inicio;

	@Override
	public void create() {
		batch = new SpriteBatch();
		inicio = new SplashScreen(this);
		setScreen(inicio);
	}

	public SpriteBatch getSB(){
		return batch;
	}

	@Override
	public void dispose() {
		super.dispose();
		this.batch.dispose();
	}
}