package com.mygdx.game.pantalla;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.*;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.Graphics.*;
import com.mygdx.game.controlador.gameController;
import com.mygdx.game.controlador.playerController;
import com.mygdx.game.modelo.pelota;


public class PantallaInicio extends Base{
    OrthographicCamera camara;
    private Stage stage;
    DisplayMode dm;
    Texture a;
    Sprite logo;
    SpriteBatch batch;
    float tiempo;
    Texture tgoopy;
    Texture tgoopy2;
    pelota goopy;
    float repeticiones;
    Music musica;

    public PantallaInicio(final ProyectoGrafica game){
        super(game);
        batch = game.getSB();
        a = new Texture("arkanoid.png");
        logo = new Sprite(a, 0, 0, a.getWidth(), a.getHeight());
        dm = Gdx.graphics.getDisplayMode();
        tiempo = 0f;
        repeticiones = 0f;
        tgoopy = new Texture("goopy.png");
        tgoopy2 = new Texture("goopy_quieto.png");
        logo.setSize(dm.width/3, dm.height/6);
        logo.setPosition(dm.width/2 - logo.getWidth()/2, dm.height - logo.getHeight() - dm.height*7/100);
        goopy = new pelota(tgoopy2, dm.width/2 - tgoopy.getWidth()/34, dm.height*0.475f,tgoopy.getWidth()/17, tgoopy.getHeight());
        goopy.animacion(tgoopy, 17, 1);
        Gdx.graphics.setFullscreenMode(dm);
        camara = new OrthographicCamera(dm.width,dm.height);
        stage = new Stage(new ScreenViewport(camara));
    }

    @Override
    public void show() {
        musica = Gdx.audio.newMusic(Gdx.files.internal("floral fury.mp3"));
        musica.setLooping(true);
        musica.setVolume(0.5f);
        musica.play();
        Gdx.input.setInputProcessor(stage);
        Skin mySkin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));
        Button button2 = new TextButton("inicio",mySkin,"default");
        button2.setPosition(dm.width/2-button2.getWidth()/2,dm.height/4);
        button2.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                musica.stop();
                stage.dispose();
                game.setScreen(new PantallaCarga(game,9, dm,5, 0));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(button2);
    }

    @Override
    public void render(float delta) {
        batch.setProjectionMatrix(camara.combined);
        tiempo += Gdx.graphics.getDeltaTime();
        repeticiones += Gdx.graphics.getDeltaTime();
        ScreenUtils.clear(0.4f, 0, 0.6f, 1);
        batch.begin();
        logo.draw(batch);
        if(repeticiones >= 5*17/25){
            if(goopy.animado){
                goopy.quieto(tgoopy2);
            }
            if(repeticiones >= 5*17/25 + 3){
                repeticiones = 0f;
            }
        }
        else{
            if(!goopy.animado){
                goopy.animacion(tgoopy, 17, 1);
            }
        }
        goopy.render(batch, tiempo);
        stage.act();
        stage.draw();
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        batch.dispose();
    }
}
