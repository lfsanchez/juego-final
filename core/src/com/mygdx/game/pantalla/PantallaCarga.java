package com.mygdx.game.pantalla;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.controlador.gameController;
import com.mygdx.game.controlador.playerController;
import com.mygdx.game.modelo.carga;
import com.mygdx.game.modelo.pelota;


public class PantallaCarga extends Base{
    DisplayMode dm;
    Texture a;
    Sprite logo;
    Texture c;
    carga spr;
    SpriteBatch batch;
    float tiempo;
    int nivel;
    Music musica;
    Music ready;
    int vidas;
    int puntaje;
    float transparencia;

    public PantallaCarga(final ProyectoGrafica game, int nivel, Graphics.DisplayMode dm1, int v, int puntos){
        super(game);
        batch = game.getSB();
        transparencia = 0f;
        this.nivel = nivel;
        vidas = v;
        puntaje = puntos;
        a = new Texture("ready.png");
        logo = new Sprite(a, 0, 0, a.getWidth(), a.getHeight());
        c = new Texture("carga.png");
        spr = new carga(c, 0, 0, c.getWidth(), c.getHeight());
        spr.animacion(c, 5, 1);
        dm = dm1;
        spr.personaje.setSize(dm.width/3, dm.height/2);
        spr.personaje.setPosition(dm.width/2 - spr.personaje.getWidth()/2,dm.height - spr.personaje.getHeight() - dm.height*0.05f);
        tiempo = 0f;
        logo.setSize(dm.width/2, dm.height/3);
        logo.setPosition(dm.width/2 - logo.getWidth()/2, dm.height/2 - logo.getHeight()/2 - dm.height*0.2f);
        logo.setAlpha(transparencia);
        Gdx.graphics.setFullscreenMode(dm);
    }

    @Override
    public void show() {
        ready = Gdx.audio.newMusic(Gdx.files.internal("areUready.mp3"));
        musica = Gdx.audio.newMusic(Gdx.files.internal("inicio.mp3"));
        musica.setVolume(0.5f);
        ready.setVolume(0.5f);
        ready.play();
        musica.play();
    }

    @Override
    public void render(float delta) {
        tiempo += Gdx.graphics.getDeltaTime();
        ScreenUtils.clear(0.4f, 0, 0.6f, 1);
        batch.begin();
        logo.draw(batch);
        spr.render(batch, tiempo/2);
        if(transparencia <= 0.98f){
            transparencia+=0.005f;
        }
        if(tiempo >= 8f){
            game.setScreen(new PantallaJuego(game,nivel, dm,vidas, puntaje, new gameController(nivel), new playerController(nivel, vidas)));
            musica.stop();
            ready.stop();
        }
        logo.setAlpha(transparencia);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
    }
}
