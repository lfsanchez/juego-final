package com.mygdx.game.pantalla;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.controlador.gameController;
import com.mygdx.game.controlador.playerController;
import com.mygdx.game.modelo.Personaje;
import com.mygdx.game.modelo.corazon;
import com.mygdx.game.modelo.flama;
import com.mygdx.game.modelo.ladrillo;
import com.mygdx.game.modelo.ladrillos;
import com.mygdx.game.modelo.pelota;
import com.mygdx.game.modelo.plataforma;
import com.mygdx.game.modelo.poder;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;

public class PantallaJuego extends Base{
    SpriteBatch batch;
    BitmapFont niv;
    BitmapFont pun;
    BitmapFont vid;
    float tiempo;
    gameController control;
    playerController player;
    Texture tgoopy2;
    pelota goopy;
    ladrillos pared;
    plataforma platform;
    int index;
    Object[] pers;
    Graphics.DisplayMode dm;
    float repeticiones;
    Music musica;
    boolean fin;
    int nivel;
    int vidas;
    int puntaje;
    LinkedList<corazon> cv;

    public PantallaJuego(final ProyectoGrafica game, int nivel, Graphics.DisplayMode dm1, int v, int puntos ,gameController control, playerController player){
        super(game);
        niv = new BitmapFont();
        pun = new BitmapFont();
        vid = new BitmapFont();
        cv = new LinkedList<>();
        fin = false;
        dm = dm1;
        for(int i=0;i<5;i++){
            Texture c = new Texture("corazon.png");
            corazon aux = new corazon(c, 0,0,c.getWidth(),c.getHeight());
            aux.personaje.setSize(dm.height*0.05f, dm.height*0.05f);
            aux.personaje.setPosition(i*aux.personaje.getWidth()-dm.width/dm.height*dm.width*0.5f+dm.width*0.575f,dm.height*0.87f);
            aux.animacion(c,6,1);
            cv.add(aux);
        }
        index = 0;
        vidas = v;
        this.nivel = nivel;
        this.puntaje = puntos;
        repeticiones = 0f;
        batch = game.getSB();
        tiempo = 0f;
        tgoopy2 = new Texture("goopy_quieto.png");
        platform = new plataforma(new Texture("plataforma.png"),dm.width/2 - dm.width*0.15f/2, dm.height*0.025f, dm.width*0.15f, dm.height*0.1f);
        goopy = new pelota(tgoopy2, dm.width/2 - dm.width * 0.0125f/2, dm.height*0.025f + dm.height*0.025f,dm.width * 0.025f, dm.width * 0.025f);
        pared = new ladrillos(String.valueOf(nivel % 10)+".txt",dm);
        this.control = control;
        this.player = player;
        pers = new Object[5];
        pers[0] = platform;
        pers[1] = goopy;
        pers[2] = pared;
        pers[3] = vidas;
        pers[4] = puntaje;
    }

    @Override
    public void show() {
        musica = Gdx.audio.newMusic(Gdx.files.internal("musica.mp3"));
        musica.setLooping(true);
        musica.setVolume(0.5f);
        musica.play();
        Gdx.input.setInputProcessor(player);
    }

    @Override
    public void render(float delta) {
        tiempo += Gdx.graphics.getDeltaTime();
        fin = false;
        ScreenUtils.clear(0.4f, 0, 0.6f, 1);
        batch.begin();
        double num = (double)dm.width/dm.height;
        num += 0.1;
        double parteDecimal = num % 1; // Lo que sobra de dividir al número entre 1
        double parteEntera = num - parteDecimal;
        float proporcion = (float) parteEntera;
        niv.getData().setScale(1+proporcion,1+dm.width/dm.height);
        vid.getData().setScale(1+proporcion,1+dm.width/dm.height);
        pun.getData().setScale(1+proporcion,1+dm.width/dm.height);
        pers = player.cambios(pers, dm);
        platform = (plataforma) pers[0];
        goopy = (pelota) pers[1];
        pared = (ladrillos) pers[2];
        pers = control.cambios(pers, dm);
        platform = (plataforma) pers[0];
        goopy = (pelota) pers[1];
        pared = (ladrillos) pers[2];
        vidas = (int) pers[3];
        puntaje = (int) pers[4];
        if(vidas <=0){
            musica.stop();
            game.setScreen(new PantallaGameOver(this.game, puntaje));
        }
        goopy.render(batch, tiempo);
        platform.render(batch, tiempo);
        while(index < pared.mapa.size()){
            if(pared.mapa.get(index).activo){
                pared.mapa.get(index).render(batch, tiempo);
                fin = true;
            }
            else{
                while(repeticiones<= 2.5f &&pared.mapa.get(index).animado){
                    pared.mapa.get(index).render(batch, tiempo);
                    repeticiones += Gdx.graphics.getDeltaTime();
                }
                pared.mapa.get(index).animado = false;
                repeticiones = 0f;
            }
            for(int i=0; i<4; i++){
                Object[] values = new Object[4];
                values[0] = pared.mapa.get(index);
                values[1] = goopy;
                values[2] = platform;
                values[3] = vidas;
                if(pared.mapa.get(index).objetos[i]){
                    values = control.objetosL(values, i, nivel, dm.height*0.025f);
                    pared.mapa.set(index, (ladrillo)values[0]);
                    goopy = (pelota) values[1];
                    platform = (plataforma) values[2];
                    vidas = (int) values[3];
                }
            }
            index++;
        }
        pared.renderObjects(batch,tiempo);
        index = 0;
        niv.draw(batch, "NIVEL "+String.valueOf(nivel),dm.width*0.01f , dm.height*0.99f);
        pun.draw(batch, "PUNTOS: "+String.valueOf(puntaje),dm.width*0.01f, dm.height*0.95f);
        vid.draw(batch, "VIDAS: ",dm.width*0.01f, dm.height*0.91f);
        for(int i=0;i<cv.size();i++){
            cv.get(i).render(batch,tiempo);
        }
        if(vidas<cv.size()){
            cv.removeLast();
        }
        else{
            if(vidas>cv.size()){
                Texture c = new Texture("corazon.png");
                corazon aux = new corazon(c, 0,0,c.getWidth(),c.getHeight());
                aux.animacion(c,6,1);
                aux.personaje.setSize(dm.height*0.05f, dm.height*0.05f);
                aux.personaje.setPosition(cv.size()*aux.personaje.getWidth()-dm.width/dm.height*dm.width*0.5f+dm.width*0.575f,dm.height*0.87f);
                cv.add(aux);
            }
        }
        pers[0] = platform;
        pers[1] = goopy;
        pers[2] = pared;
        pers[3] = vidas;
        pers[4] = puntaje;
        if(!fin){
            musica.stop();
            game.setScreen(new PantallaCarga(game,nivel+1, dm, vidas, puntaje));
        }
        batch.end();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
    }
}
