package com.mygdx.game.pantalla;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.controlador.gameController;
import com.mygdx.game.controlador.playerController;
import com.mygdx.game.modelo.pelota;


public class PantallaGameOver extends Base{
    OrthographicCamera camara;
    private Stage stage;
    DisplayMode dm;
    Texture a;
    Sprite logo;
    SpriteBatch batch;
    float tiempo;
    Texture tgoopy;
    Sprite lapida;
    float repeticiones;
    Music musica;
    BitmapFont p;
    int puntos;

    public PantallaGameOver(final ProyectoGrafica game, int puntos){
        super(game);
        this.puntos = puntos;
        p = new BitmapFont();
        batch = game.getSB();
        dm = Gdx.graphics.getDisplayMode();
        Gdx.audio.newMusic(Gdx.files.internal("sgo.mp3")).play();
        a = new Texture("game over.png");
        logo = new Sprite(a, 0, 0, a.getWidth(), a.getHeight());
        logo.setSize(dm.width*0.3f, dm.height*0.4f);
        logo.setPosition(dm.width/2-dm.width*0.3f/2, dm.height-dm.height*0.4f);
        tiempo = 0f;
        repeticiones = 0f;
        tgoopy = new Texture("lapida.png");
        lapida = new Sprite(tgoopy, 0, 0, tgoopy.getWidth(), tgoopy.getHeight());
        lapida.setPosition(dm.width/2-tgoopy.getWidth()/2,dm.height*0.1f+dm.height/12);
        camara = new OrthographicCamera(dm.width,dm.height);
        stage = new Stage(new ScreenViewport(camara));
        Gdx.input.setInputProcessor(stage);
        musica = Gdx.audio.newMusic(Gdx.files.internal("trials.mp3"));
        musica.setLooping(true);
        musica.setVolume(0.5f);
        musica.play();
        Skin mySkin = new Skin(Gdx.files.internal("skin/glassy-ui.json"));
        Button button2 = new TextButton("Empezar de nuevo",mySkin,"default");
        button2.setSize(dm.width/2, dm.height/12);
        button2.setPosition(dm.width/2-dm.width/6,dm.height*0.05f);
        button2.addListener(new InputListener(){
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                musica.stop();
                game.setScreen(new PantallaInicio(game));
            }
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
        });
        stage.addActor(button2);
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        p.getData().setScale(5,5);
        batch.setProjectionMatrix(camara.combined);
        tiempo += Gdx.graphics.getDeltaTime();
        repeticiones += Gdx.graphics.getDeltaTime();
        ScreenUtils.clear(0.4f, 0, 0.6f, 1);
        batch.begin();
        String mensaje = "PUNTUACION FINAL: "+String.valueOf(puntos);
        p.draw(batch, mensaje ,dm.width/2 - mensaje.length()*20, dm.height*0.6f);
        logo.draw(batch);
        lapida.draw(batch);
        stage.act();
        stage.draw();
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        batch.dispose();
    }
}
