package com.mygdx.game.pantalla;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.mygdx.game.controlador.gameController;
import com.mygdx.game.controlador.playerController;
import com.mygdx.game.modelo.pelota;


public class SplashScreen extends Base{
    DisplayMode dm;
    Texture a;
    Sprite logo;
    SpriteBatch batch;
    float transparencia;
    float tiempo;
    boolean baja;
    private PantallaInicio inicio;
    OrthographicCamera camara;

    public SplashScreen(ProyectoGrafica game){
        super(game);
        batch = game.getSB();
        a = new Texture("banner.png");
        logo = new Sprite(a, 0, 0, a.getWidth(), a.getHeight());
        dm = Gdx.graphics.getDisplayMode();
        Gdx.graphics.setFullscreenMode(dm);
        camara = new OrthographicCamera(dm.width,dm.height);
        logo.setPosition(-dm.width/2, -dm.height/2);
        logo.setSize(dm.width, dm.height);
        logo.setAlpha(0f);
        transparencia = 0f;
        tiempo = 0f;
        baja = false;
        inicio = new PantallaInicio(game);

    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        batch.setProjectionMatrix(camara.combined);
        tiempo += delta;
        ScreenUtils.clear(0f, 0, 0f, 1);
        batch.begin();
        logo.draw(batch);
        if(transparencia <= 0.98f && !baja){
            transparencia+=0.01f;
        }
        if(tiempo >= 2.5f){
            baja = true;
        }
        if(baja && transparencia >= 0.02f){
            transparencia -= 0.01f;
        }
        if(baja && transparencia < 0.02f){
            game.setScreen(inicio);
        }
        logo.setAlpha(transparencia);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
    }
}
